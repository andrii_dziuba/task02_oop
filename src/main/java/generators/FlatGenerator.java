package generators;

import model.Flat;
import model.Habitat;

import java.util.concurrent.ThreadLocalRandom;

public class FlatGenerator implements IGenerator {
    @Override
    public Habitat randomHabitat() {
        Flat flat = new Flat();
        this.generateSuperHabitat(flat);

        flat.setFloor(ThreadLocalRandom.current().nextInt(16));

        return flat;
    }
}
