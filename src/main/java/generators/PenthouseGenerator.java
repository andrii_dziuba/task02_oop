package generators;

import model.Habitat;
import model.Penthouse;

import java.util.concurrent.ThreadLocalRandom;

public class PenthouseGenerator implements IGenerator {

    @Override
    public Habitat randomHabitat() {
        Penthouse penthouse = new Penthouse();
        this.generateSuperHabitat(penthouse);

        penthouse.setHasIndividualElevator(ThreadLocalRandom.current().nextBoolean());
        penthouse.setHasWaterpool(ThreadLocalRandom.current().nextBoolean());

        return penthouse;
    }
}
