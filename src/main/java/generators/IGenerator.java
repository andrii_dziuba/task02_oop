package generators;

import model.Habitat;

import java.util.UUID;

import static java.util.concurrent.ThreadLocalRandom.current;

public interface IGenerator {
    Habitat randomHabitat();

    /**
     * Default method to randomly generate data of <code>Habitat</code> fields
     * @param habitat
     * @return
     */
    default Habitat generateSuperHabitat(Habitat habitat) {
        if (habitat == null) {
            return null;
        }

        habitat.setChambersCount(current().nextInt(5));
        habitat.setDistToChildGarden(current().nextFloat() * current().nextInt(5));
        habitat.setDistToNearestHospital(current().nextFloat() * current().nextInt(5));
        habitat.setDistToNearestPark(current().nextFloat() * current().nextInt(5));
        habitat.setDistToNearestSchool(current().nextFloat() * current().nextInt(5));
        habitat.setHabitatArea(current().nextFloat() * current().nextInt(120));
        habitat.setRent(current().nextInt(50) * 10);
        habitat.setAddress("Lviv, " + UUID.randomUUID().toString().substring(0, 8) + " str, No: " + current().nextInt(120));

        return habitat;
    }
}
