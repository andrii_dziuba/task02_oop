package generators;

import model.Chamber;
import model.Habitat;

import java.util.concurrent.ThreadLocalRandom;

public class ChamberGenerator implements IGenerator {

    @Override
    public Habitat randomHabitat() {
        Chamber chamber = new Chamber();
        this.generateSuperHabitat(chamber);

        chamber.setFloor(ThreadLocalRandom.current().nextInt(16));
        chamber.setChambersCount(1);

        return chamber;
    }
}
