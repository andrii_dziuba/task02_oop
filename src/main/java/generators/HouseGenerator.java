package generators;

import model.Habitat;
import model.House;

import java.util.concurrent.ThreadLocalRandom;

public class HouseGenerator implements IGenerator {

    @Override
    public Habitat randomHabitat() {
        House house = new House();
        this.generateSuperHabitat(house);

        house.setFloorCount(ThreadLocalRandom.current().nextInt(3));
        house.setHasBasement(ThreadLocalRandom.current().nextBoolean());
        house.setChambersCount(house.getFloorCount() * ThreadLocalRandom.current().nextInt(3));

        return house;
    }
}
