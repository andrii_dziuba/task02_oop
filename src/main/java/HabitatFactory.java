import generators.ChamberGenerator;
import generators.FlatGenerator;
import generators.HouseGenerator;
import generators.PenthouseGenerator;
import model.*;

import java.util.Objects;

/**
 * Class-factory of the randomly generated habitat by type
 * @author Dziuba Andrii
 */
public class HabitatFactory {

    /**
     * Generates randomly habitat by type
     * @param cl - <T extends Habitat>
     * @return instance of T
     */
    public Habitat generateHabitatByType(Class cl) {
        Objects.requireNonNull(cl);
        String className = cl.getSimpleName();
        Habitat habitat = null;

        if (className.equals(Chamber.class.getSimpleName())) {
            habitat = new ChamberGenerator().randomHabitat();
        } else if (className.equals(Flat.class.getSimpleName())) {
            habitat = new FlatGenerator().randomHabitat();
        } else if (className.equals(House.class.getSimpleName())) {
            habitat = new HouseGenerator().randomHabitat();
        } else if (className.equals(Penthouse.class.getSimpleName())) {
            habitat = new PenthouseGenerator().randomHabitat();
        }
        return habitat;
    }
}
