import java.util.List;
import java.util.Scanner;

/**
 * This class designed for drawing user menu, listening user commands and execute appropriate actions
 * @author Dziuba Andrii
 */
public class UserMenu {

    /**
     * Create an instance of the class
     */
    public UserMenu() {
        printMenu();
    }

    /**
     * Method, which performs main purpose of this class
     */
    private void printMenu() {
        System.out.println("Greetings User. I you are looking for the new home, we have some variants for you!");
        while (true) {
            System.out.println("Please type associated number to should we know what type of habitats you would prefer");

            int typedNumber = 0;
            Scanner scanner = new Scanner(System.in);
            do {
                System.out.println("1 - House\n" +
                        "2 - Penthouse\n" +
                        "3 - Flat\n" +
                        "4 - Chamber\n" +
                        "5 - whole of them\n" +
                        "6 - for exit");
                if (scanner.hasNext()) {
                    typedNumber = scanner.nextInt();
                }
            } while (typedNumber < 1 || typedNumber > 6);
            List list = null;
            HabitatHandler hh = MainApplication.habitatHandler;
            String habitatType = null;

            switch (typedNumber) {
                case 1:
                    list = hh.getAllHouses();
                    habitatType = "house";
                    break;
                case 2:
                    list = hh.getAllPenthouses();
                    habitatType = "penthouse";
                    break;
                case 3:
                    list = hh.getAllFlats();
                    habitatType = "flat";
                    break;
                case 4:
                    list = hh.getAllChambers();
                    habitatType = "chamber";
                    break;
                case 5:
                    list = hh.getAllHabitats();
                    habitatType = "all types";
                    break;
                case 6:
                    System.out.println("Good bye!");
                    return;
            }

            System.out.println("You have chosen \"" + habitatType + "\"");
            System.out.println("Now you might filter them by few criteria, please, choose:");
            typedNumber = 0;
            do {
                System.out.println("1 - rent cost\n" +
                        "2 - habitat area");
                if (scanner.hasNext()) {
                    typedNumber = scanner.nextInt();
                }
            } while (typedNumber < 1 || typedNumber > 2);
            System.out.println("You have chosen " + typedNumber);
            int condition = 0;

            do {
                System.out.println("Now type number of contidion:\n1 - less than\n2 - greater than");
                if (scanner.hasNext()) {
                    condition = scanner.nextInt();
                }
            } while (condition < 1 || condition > 2);
            System.out.println("Ok. Type the value of condition that you have chosen earlier");
            int value = 0;
            do {
                if (scanner.hasNext()) {
                    value = scanner.nextInt();
                }
            } while (value < 0);

            if (typedNumber == 1 && condition == 1) {
                list = HabitatSorter.rentCostsLessThan(list, value);
            }
            if (typedNumber == 1 && condition == 2) {
                list = HabitatSorter.rentCostsGreaterThan(list, value);
            }
            if (typedNumber == 2 && condition == 1) {
                list = HabitatSorter.habitatAreaLessThan(list, value);
            }
            if (typedNumber == 2 && condition == 2) {
                list = HabitatSorter.habitatAreaGreaterThan(list, value);
            }
            if (list.size() == 0) {
                System.out.println("We don\'t have habitats in our database that match your conditions");
            } else {
                list.forEach(System.out::println);
            }

            condition = 0;
            do {
                System.out.println("Now you could sort them by remoteness to:\n" +
                        "1 - hospital\n" +
                        "2 - park");
                if (scanner.hasNext()) {
                    condition = scanner.nextInt();
                }
            } while (condition < 1 || condition > 2);

            if (condition == 1) {
                list = HabitatSorter.sortByDistannceToNearestHospital(list);
            }
            if (condition == 2) {
                list = HabitatSorter.sortByDistannceToNearestPark(list);
            }

            if (list.size() == 0) {
                System.out.println("We don\'t have habitats in our database that match your conditions");
            } else {
                list.forEach(System.out::println);
            }
        }
    }


}
