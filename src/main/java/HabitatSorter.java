import model.Habitat;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created for realize utils methods of sorting and filtering lists by certain criteria
 */
public class HabitatSorter {

    /**
     * Filters <code>list</code> by criteria of distance to nearest hospital
     * @param habitats - list for filtering
     * @return filtered list
     */
    public static List<Habitat> sortByDistannceToNearestHospital(List<Habitat> habitats) {
        habitats.sort(new Comparator<Habitat>() {
            @Override
            public int compare(Habitat o1, Habitat o2) {
                return (o1.getDistToNearestHospital() > o2.getDistToNearestHospital()) ? 1 : -1;
            }
        });
        return habitats;
    }

    /**
     * Filters <code>list</code> by criteria of distance to nearest park
     * @param habitats - list for filtering
     * @return filtered list
     */
    public static List<Habitat> sortByDistannceToNearestPark(List<Habitat> habitats) {
        habitats.sort(new Comparator<Habitat>() {
            @Override
            public int compare(Habitat o1, Habitat o2) {
                return (o1.getDistToNearestPark() > o2.getDistToNearestPark()) ? 1 : -1;
            }
        });
        return habitats;
    }

    /**
     * Filters the list of habitats that rent cost are less then <code>rent</code>
     * @param habitats - list of habitats to filter
     * @param rent - <code>int</code> value
     * @return filtered list
     */
    public static List<Habitat> rentCostsLessThan(List<Habitat> habitats, int rent) {
        return habitats
                .stream()
                .filter(habitat -> habitat.getRent() < rent)
                .collect(Collectors.toList());
    }

    /**
     * Filters the list of habitats that rent cost are greater then <code>rent</code>
     * @param habitats - list of habitats to filter
     * @param rent - <code>int</code> value
     * @return filtered list
     */
    public static List<Habitat> rentCostsGreaterThan(List<Habitat> habitats, int rent) {
        return habitats
                .stream()
                .filter(habitat -> habitat.getRent() > rent)
                .collect(Collectors.toList());
    }

    /**
     * Filters the list of habitats in which area are less then <code>area</code>
     * @param habitats - list of habitats to filter
     * @param area - <code>int</code> value
     * @return filtered list
     */
    public static List<Habitat> habitatAreaLessThan(List<Habitat> habitats, int area) {
        return habitats
                .stream()
                .filter(habitat -> habitat.getRent() < area)
                .collect(Collectors.toList());
    }

    /**
     * Filters the list of habitats in which area are less then <code>area</code>
     * @param habitats - list of habitats to filter
     * @param area - <code>int</code> value
     * @return filtered list
     */
    public static List<Habitat> habitatAreaGreaterThan(List<Habitat> habitats, int area) {
        return habitats
                .stream()
                .filter(habitat -> habitat.getRent() > area)
                .collect(Collectors.toList());
    }

}
