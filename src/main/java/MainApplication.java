import model.Chamber;
import model.Flat;
import model.House;
import model.Penthouse;

import java.util.stream.Stream;

/**
 * Main class of this program
 * @author Dziuba Andrii
 */
public class MainApplication {

    public static HabitatHandler habitatHandler = new HabitatHandler();

    /**
     * Entry point of the application
     * @param args
     */
    public static void main(String[] args) {
        generateRandomHabitats();
        UserMenu um = new UserMenu();

    }

    /**
     * Helper method to generate random habitats and add them to main list
     * (For lazy)
     */
    public static void generateRandomHabitats() {
        Stream.generate(() -> new HabitatFactory().generateHabitatByType(Chamber.class)).limit(15).forEach(habitatHandler::add);
        Stream.generate(() -> new HabitatFactory().generateHabitatByType(Flat.class)).limit(15).forEach(habitatHandler::add);
        Stream.generate(() -> new HabitatFactory().generateHabitatByType(Penthouse.class)).limit(15).forEach(habitatHandler::add);
        Stream.generate(() -> new HabitatFactory().generateHabitatByType(House.class)).limit(15).forEach(habitatHandler::add);

        habitatHandler.shuffle();
    }

}
