import model.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class which stores list of all habitats and operate them
 * @author Dziuba Andrii
 */
public class HabitatHandler {

    private volatile List<Habitat> allHabitats = null;

    /**
     * Constructor
     */
    public HabitatHandler() {
        allHabitats = new ArrayList<>();
    }

    /**
     * Public method to add another habitat to the list
     * @param habitat
     */
    public void add(Habitat habitat) {
        allHabitats.add(habitat);
    }

    /**
     * Shuffles the list
     */
    public void shuffle() {
        Collections.shuffle(allHabitats);
    }

    /**
     * Filters the habitat list by type of habitat. Chamber in this case
     * @return list of chambers
     */
    public List<Chamber> getAllChambers() {
        return allHabitats
                .stream()
                .filter(habitat -> habitat instanceof Chamber)
                .map(i -> (Chamber) i)
                .collect(Collectors.toList());
    }

    /**
     * Filters the habitat list by type of habitat. Flat in this case
     * @return list of flats
     */
    public List<Flat> getAllFlats() {
        return allHabitats
                .stream()
                .filter(habitat -> habitat instanceof Flat)
                .map(i -> (Flat) i)
                .collect(Collectors.toList());
    }

    /**
     * Filters the habitat list by type of habitat. House in this case
     * @return list of houses
     */
    public List<House> getAllHouses() {
        return allHabitats
                .stream()
                .filter(habitat -> habitat instanceof House)
                .map(i -> (House) i)
                .collect(Collectors.toList());
    }

    /**
     * Filters the habitat list by type of habitat. Penthouse in this case
     * @return list of penthouses
     */
    public List<Penthouse> getAllPenthouses() {
        return allHabitats
                .stream()
                .filter(habitat -> habitat instanceof Penthouse)
                .map(i -> (Penthouse) i)
                .collect(Collectors.toList());
    }

    /**
     * @return list of all habitats
     */
    public List<Habitat> getAllHabitats() {
        return allHabitats;
    }
}
