package model;

/**
 * Class which stores information about Flat
 * @author Dziuba Andrii
 */
public class Flat extends Habitat {

    private int floor;

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    @Override
    public String toString() {
        return "Flat{" +
                "floor=" + floor +
                " " + super.toString();
    }
}
