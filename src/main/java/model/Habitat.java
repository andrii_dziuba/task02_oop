package model;

/**
 * Abstract class which stores information about Habitat
 * @author Dziuba Andrii
 */
public abstract class Habitat {

    private int rent;
    private float distToChildGarden;
    private float distToNearestPark;
    private float distToNearestSchool;
    private float distToNearestHospital;
    private float habitatArea;
    private int chambersCount;
    private String address;

    public int getRent() {
        return rent;
    }

    public void setRent(int rent) {
        this.rent = rent;
    }

    public float getDistToChildGarden() {
        return distToChildGarden;
    }

    public void setDistToChildGarden(float distToChildGarden) {
        this.distToChildGarden = distToChildGarden;
    }

    public float getDistToNearestPark() {
        return distToNearestPark;
    }

    public void setDistToNearestPark(float distToNearestPark) {
        this.distToNearestPark = distToNearestPark;
    }

    public float getDistToNearestSchool() {
        return distToNearestSchool;
    }

    public void setDistToNearestSchool(float distToNearestSchool) {
        this.distToNearestSchool = distToNearestSchool;
    }

    public float getDistToNearestHospital() {
        return distToNearestHospital;
    }

    public void setDistToNearestHospital(float distToNearestHospital) {
        this.distToNearestHospital = distToNearestHospital;
    }

    public float getHabitatArea() {
        return habitatArea;
    }

    public void setHabitatArea(float habitatArea) {
        this.habitatArea = habitatArea;
    }

    public int getChambersCount() {
        return chambersCount;
    }

    public void setChambersCount(int chambersCount) {
        this.chambersCount = chambersCount;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "address=" + address +
                ", rent=" + rent + "$" +
                ", distToChildGarden=" + distToChildGarden + "km" +
                ", distToNearestPark=" + distToNearestPark + "km" +
                ", distToNearestSchool=" + distToNearestSchool + "km" +
                ", distToNearestHospital=" + distToNearestHospital + "km" +
                ", habitatArea=" + habitatArea + "m2" +
                ", chambersCount=" + chambersCount +
                '}';
    }
}
