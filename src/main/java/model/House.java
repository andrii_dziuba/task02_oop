package model;

/**
 * Class which stores information about House
 * @author Dziuba Andrii
 */
public class House extends Habitat {

    private int floorCount;
    private boolean hasBasement;

    public int getFloorCount() {
        return floorCount;
    }

    public void setFloorCount(int floorCount) {
        this.floorCount = floorCount;
    }

    public boolean isHasBasement() {
        return hasBasement;
    }

    public void setHasBasement(boolean hasBasement) {
        this.hasBasement = hasBasement;
    }

    @Override
    public String toString() {
        return "House{" +
                "floorCount=" + floorCount +
                ", hasBasement=" + hasBasement +
                " " + super.toString();
    }
}
