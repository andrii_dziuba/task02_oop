package model;

/**
 * Class which stores information about Penthouse
 * @author Dziuba Andrii
 */
public class Penthouse extends Habitat {

    private boolean hasIndividualElevator;
    private boolean hasWaterpool;

    public boolean isHasIndividualElevator() {
        return hasIndividualElevator;
    }

    public void setHasIndividualElevator(boolean hasIndividualElevator) {
        this.hasIndividualElevator = hasIndividualElevator;
    }

    public boolean isHasWaterpool() {
        return hasWaterpool;
    }

    public void setHasWaterpool(boolean hasWaterpool) {
        this.hasWaterpool = hasWaterpool;
    }

    @Override
    public String toString() {
        return "Penthouse{" +
                "hasIndividualElevator=" + hasIndividualElevator +
                ", hasWaterpool=" + hasWaterpool +
                " " + super.toString();
    }
}
