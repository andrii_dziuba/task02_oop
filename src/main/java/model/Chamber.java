package model;

/**
 * Class which stores information about Chamber
 * @author Dziuba Andrii
 */
public class Chamber extends Habitat {

    private int floor;

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    @Override
    public String toString() {
        return "Chamber{" +
                "floor=" + floor +
                " " + super.toString();
    }
}
